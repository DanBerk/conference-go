from django.http import JsonResponse
from .models import Presentation, Status
from .encoders import PresentationListEncoder, PresentationDetailEncoder
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
import pika


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    presentations = Presentation.objects.filter(conference=conference_id)
    if request.method == "GET":
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe="False",
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    try:
        presentation = Presentation.objects.get(id=id)
        if request.method == "GET":
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            count, _ = Presentation.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
            return JsonResponse(
                {"message": "Presentation does not exist"},
                status=400,
            )
        else:
            content = json.loads(request.body)
            presentation = Presentation.objects.get(id=id)
            try:
                if content.get("status"):
                    status = Status.objects.get(name=content.get("status"))
                    content["status"] = status
            except Status.DoesNotExist:
                return JsonResponse(
                    {
                        "message": "Invalid Status, use APPROVED, REJECTED, SUBMITTED"
                    }
                )
            try:
                if content.get("conference"):
                    conference = Conference.objects.get(
                        id=content.get("conference")
                    )
                    content["conference"] = conference
            except Conference.DoesNotExist:
                return JsonResponse({"message": "Invalid Conference Name"})
            Presentation.objects.filter(id=id).update(**content)
            presentation = Presentation.objects.get(id=id)
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
    except Presentation.DoesNotExist:
        return JsonResponse(
            {"message": "Presentation does not exist"},
            status=400,
        )


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_approvals",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        ),
    )
    connection.close()
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejection")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_rejection",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        ),
    )
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
