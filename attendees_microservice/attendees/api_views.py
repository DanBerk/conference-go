from django.http import JsonResponse
from .models import Attendee, ConferenceVO
from .encoders import (
    AttendeesListEncoder,
    AttendeesDetailEncoder,
)
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    attendees = Attendee.objects.filter(conference=conference_vo_id)
    if request.method == "GET":
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeesListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeesDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
        if request.method == "GET":
            return JsonResponse(
                attendee,
                encoder=AttendeesDetailEncoder,
                safe=False,
            )
        elif request.method == "DELETE":
            count, _ = Attendee.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        else:
            content = json.loads(request.body)
            try:
                attendee = Attendee.objects.get(id=id)
            except Attendee.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid attendee id"},
                    status=400,
                )
            Attendee.objects.filter(id=id).update(**content)
            attendee = Attendee.objects.get(id=id)
            return JsonResponse(
                attendee,
                encoder=AttendeesDetailEncoder,
                safe=False,
            )
    except Attendee.DoesNotExist:
        return JsonResponse(
            {"message": "Attendee does not exist"},
            status=400,
        )
